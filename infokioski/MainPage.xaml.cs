﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace infokioski
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        DispatcherTimer Timer = new DispatcherTimer();

        public void createTimer()
        {
            InitializeComponent();
            DataContext = this;
            Timer.Tick += Timer_Tick;
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
        }

        private void Timer_Tick(object sender, object e)
        {
            Time.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }


        public MainPage()
        {
            this.InitializeComponent();
            frame1.Navigate(typeof(Tunniplaan));
            this.createTimer();
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (e.AddedItems[0].ToString())
            {
                case "Tunniplaan":
                    frame1.Navigate(typeof(Tunniplaan));
                    break;
                case "Huviringid":
                    frame1.Navigate(typeof(Huviringid));
                    break;
                case "Uudised":
                    frame1.Navigate(typeof(Uudised));
                    break;
                case "Majaplaan":
                    frame1.Navigate(typeof(Majaplaan));
                    break;
                case "Bussigraafik":
                    frame1.Navigate(typeof(Bussigraafik));
                    break;
            }
            
        }
    }
}
